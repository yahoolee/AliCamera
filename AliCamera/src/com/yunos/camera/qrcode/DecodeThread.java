
package com.yunos.camera.qrcode;

import com.yunos.camera.CameraActivity;
import com.yunos.camera.ScanModule;

import android.os.Handler;
import android.os.Looper;

import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

/**
 * This thread does all the heavy lifting of decoding the images.
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 */
public class DecodeThread extends Thread {

    public static final String BARCODE_BITMAP = "barcode_bitmap";

    private final ScanModule mModule;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;

    public DecodeThread(CameraActivity mActivity, ScanModule mModule) {

        this.mModule = mModule;
        handlerInitLatch = new CountDownLatch(1);


        // // The prefs can't change while the thread is running, so pick them
        // up once here.
        // if (decodeFormats == null || decodeFormats.isEmpty()) {
        // SharedPreferences prefs =
        // PreferenceManager.getDefaultSharedPreferences(activity);
        // decodeFormats = new Vector<BarcodeFormat>();
        // if (prefs.getBoolean(PreferencesActivity.KEY_DECODE_1D, true)) {
        // decodeFormats.addAll(DecodeFormatManager.ONE_D_FORMATS);
        // }
        // if (prefs.getBoolean(PreferencesActivity.KEY_DECODE_QR, true)) {
        // decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
        // }
        // if (prefs.getBoolean(PreferencesActivity.KEY_DECODE_DATA_MATRIX,
        // true)) {
        // decodeFormats.addAll(DecodeFormatManager.DATA_MATRIX_FORMATS);
        // }
        // }
    }

    public Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }

    @Override
    public void run() {
        Looper.prepare();
        handler = new DecodeHandler(mModule);
        handlerInitLatch.countDown();
        Looper.loop();
    }

}
